package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"

	log "github.com/sirupsen/logrus"

	"gitlab.com/pgarin/status/internal/let"
	"gitlab.com/pgarin/status/internal/watcher"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	log.SetOutput(os.Stderr)

	patches := make(chan let.Patch, 64)
	go watcher.Clock(ctx, patches)
	go watcher.Power(ctx, patches)
	go watcher.VPN(ctx, patches)
	go watcher.XKB(ctx, patches)

	status := let.Status{}
	for {
		select {
		case patch := <-patches:
			status = patch(status)
			render(status)
		case <-ctx.Done():
			return
		}
	}
}

func render(s let.Status) {
	if !s.IsCompleted() {
		return
	}
	fmt.Printf("%s %s %s | %s\n", s.XKB, s.VPN, s.Power, s.Clock)
}
