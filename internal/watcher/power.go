package watcher

import (
	"context"
	"os/exec"
	"strings"
	"time"
	"unsafe"

	log "github.com/sirupsen/logrus"

	"gitlab.com/pgarin/status/internal/let"
)

func Power(ctx context.Context, patches chan<- let.Patch) {
	battery := getBattery()
	if battery == "" {
		log.Info("battery wasn't detected")
		return
	}
	log.WithField("path", battery).Info("battery was detected")

	patch := func(status let.Status) let.Status {
		status.Power = getPower(battery)
		return status
	}

	patches <- patch

	ticker := time.NewTicker(let.RefreshRate.Power)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			patches <- patch
		case <-ctx.Done():
			return
		}
	}
}

func getPower(battery string) string {
	rawOutput, err := exec.Command("upower", "-i", battery).Output()
	if err != nil {
		log.WithError(err).Error("can't exec upower -i")
	}

	output := *(*string)(unsafe.Pointer(&rawOutput))
	for _, line := range strings.Split(output, "\n") {
		line = strings.TrimSpace(line)
		parts := strings.SplitN(line, " ", 2)
		if len(parts) != 2 {
			continue
		}
		parts[0] = strings.TrimSpace(parts[0])
		parts[1] = strings.TrimSpace(parts[1])

		if parts[0] == "percentage:" {
			return parts[1]
		}
	}
	log.WithField("upower -i", output).Error("can't get power percentage")
	return "-1%"
}

func getBattery() string {
	rawOutput, err := exec.Command("upower", "-e").Output()
	if err != nil {
		log.WithError(err).Error("can't exec upower -e")
	}

	output := *(*string)(unsafe.Pointer(&rawOutput))
	for _, line := range strings.Split(output, "\n") {
		line = strings.TrimSpace(line)
		if strings.Contains(line, "BAT") {
			return line
		}
	}
	return ""
}
