package watcher

import (
	"context"
	"time"

	"gitlab.com/pgarin/status/internal/let"
)

func Clock(ctx context.Context, patches chan<- let.Patch) {
	patch := func(status let.Status) let.Status {
		status.Clock = getClock()
		return status
	}

	patches <- patch

	ticker := time.NewTicker(let.RefreshRate.Clock)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			patches <- patch
		case <-ctx.Done():
			return
		}
	}
}

func getClock() string {
	t := time.Now()
	return t.Format("01/02 03:04 pm")
}
