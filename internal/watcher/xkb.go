package watcher

import (
	"context"
	"os/exec"
	"strings"
	"time"
	"unsafe"

	log "github.com/sirupsen/logrus"

	"gitlab.com/pgarin/status/internal/let"
)

func XKB(ctx context.Context, patches chan<- let.Patch) {
	patch := func(status let.Status) let.Status {
		status.XKB = getXKB()
		return status
	}

	patches <- patch

	ticker := time.NewTicker(let.RefreshRate.XKB)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			patches <- patch
		case <-ctx.Done():
			return
		}
	}
}

func getXKB() string {
	rawOutput, err := exec.Command("xkblayout-state", "print", "%n").Output()
	if err != nil {
		log.WithError(err).Error("can't exec xkblayout-state print %n")
	}

	output := *(*string)(unsafe.Pointer(&rawOutput))
	return strings.ToLower(output)
}
