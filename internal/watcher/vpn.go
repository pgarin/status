package watcher

import (
	"context"
	"os/exec"
	"strings"
	"time"
	"unsafe"

	log "github.com/sirupsen/logrus"

	"gitlab.com/pgarin/status/internal/let"
)

func VPN(ctx context.Context, patches chan<- let.Patch) {
	patch := func(status let.Status) let.Status {
		status.VPN = getVPN()
		return status
	}

	patches <- patch

	ticker := time.NewTicker(let.RefreshRate.VPN)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			patches <- patch
		case <-ctx.Done():
			return
		}
	}
}

func getVPN() string {
	rawOutput, err := exec.Command("ps", "aux").Output()
	if err != nil {
		log.WithError(err).Error("can't exec ps aux")
	}

	output := *(*string)(unsafe.Pointer(&rawOutput))
	for _, line := range strings.Split(output, "\n") {
		const marker = "sudo openvpn --config"
		if strings.Contains(line, marker) {
			parts := strings.Split(line, " ")
			vpn := parts[len(parts)-1]
			return vpn[:len(vpn)-5] // .ovpn
		}
	}
	return "net"
}
