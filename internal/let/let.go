package let

import "time"

type Patch = func(Status) Status

type Status struct {
	Clock string
	Power string
	VPN   string
	XKB   string
}

func (s Status) IsCompleted() bool {
	if s.Clock == "" {
		return false
	}
	if s.Power == "" {
		return false
	}
	if s.VPN == "" {
		return false
	}
	if s.XKB == "" {
		return false
	}
	return true
}

var RefreshRate = struct {
	Clock time.Duration
	Power time.Duration
	VPN   time.Duration
	XKB   time.Duration
}{
	Clock: time.Second * 15,
	Power: time.Second * 15,
	VPN:   time.Second,
	XKB:   time.Millisecond * 500,
}
